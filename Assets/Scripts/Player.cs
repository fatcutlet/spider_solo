﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject arrow;
    public GameObject webExplosion;
    public GameObject onTouchVisual;

    public GameManager manager;

    public AudioClip webSound;
    public AudioClip damageSound;
    public AudioClip winSound;
    public AudioClip loseSound;
    public AudioClip webRipping;

    public Transform arrowSprite;
    public Transform checkRight;
    public Transform checkTop;
    public Transform checkLeft;
    public Transform checkBottom;

    public Canvas UI;
    public GameObject[] uiInteractives;

    public float minBodyVelocity = .1f;
    [Range(.00001f, 0.5f)]
    public float webCastSpeed = .1f;
    [Range(0f, 1f)]
    public float pullSpeed = 1f;
    [Range(0f, 1f)]
    public float alignSpeed = .5f;
    public float minDistance = 0.4f; //depends from player's size
    //health of the player
    public int playerHealth = 1;

    public LayerMask whatIsGround;
    public LayerMask whatCanTearWeb;

    [HideInInspector]
    public bool isAlive;

    private Animator _animator;
    private AudioSource _audio;
    private Collider2D _coll2d;
    private DistanceJoint2D _dJoint2d;
    private Rigidbody2D _rb2d;
    private GameObject _onTouchVisualExmp;
    private LineRenderer _onTouchLine;
    private LineRenderer _webRend;
    private Vector2 _touchStart = Vector2.zero;
    private Vector2 _touchEnd = Vector2.zero;
    private Coroutine _coroutineMoving;
    private GameManager _gameManager;
    private bool _touchStarted = false;
    private bool _canMove;

    #region unity behaviour messages
    void Start()
    {
        _coll2d = GetComponent<Collider2D>();
        _webRend = GetComponent<LineRenderer>();
        _dJoint2d = GetComponent<DistanceJoint2D>();
        _rb2d = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        _webRend.enabled = false;
        _dJoint2d.enabled = false;
        arrow.SetActive(false);
        _canMove = true;
        isAlive = true;
    }

    void Update()
    {
        UpdateWeb();
        UpdatePosition();
    }

    private void LateUpdate()
    {
        if(_canMove && isAlive && !_gameManager.IsGamePaused())
            DoMove();
    }
    #endregion

    #region public methods
    //public function to apply damage to the player
    public void ApplyDamage(int damage)
    {
        if (isAlive)
        {
            playerHealth -= damage;
            PlaySfx(damageSound);

            if (playerHealth <= 0)
            {
                StartCoroutine(KillThePlayer());
            }
        }
    }

    public void OnVictory()
    {
        if (isAlive)
        {
            Freeze();
            _webRend.enabled = false;
            PlaySfx(winSound);
            isAlive = false;
        }
    }

    public void AddScore(int score, AudioClip clip)
    {
        if (isAlive) {
            manager.UpdateScore(score);
            if (clip != null)
                PlaySfx(clip);
        }
    }

    //play sfx with randomize pitch
    public void PlaySfx(AudioClip clip)
    {
        float randomPitch = Random.Range(.95f, 1.05f);
        _audio.pitch = randomPitch;
        _audio.PlayOneShot(clip);
    }
    #endregion

    #region protected corutines
    //kill the player
    protected IEnumerator KillThePlayer()
    {
        if (isAlive)
        {
            if (_coroutineMoving != null)
            {
                StopCoroutine(_coroutineMoving);
                _dJoint2d.enabled = false;
                _webRend.enabled = false;
            }

            _canMove = false;
            isAlive = false;
            arrow.SetActive(false);
            _animator.SetBool("Death", true);

            yield return new WaitForSeconds(2f);

            PlaySfx(loseSound);
            _gameManager.GameOver();
        }
        yield return null;
    }

    //draw the web smoothly
    protected IEnumerator SmoothWebDrawning(RaycastHit2D raycastHit2D)
    {
        _canMove = false;
        _webRend.enabled = true;
        Vector3 end = raycastHit2D.point;

        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
        Vector3 newPosition = Vector3.MoveTowards(transform.position, end, webCastSpeed);

        _webRend.SetPosition(1, newPosition);
        _webRend.SetPosition(0, transform.position);

        yield return null;

        while (sqrRemainingDistance > float.Epsilon)
        {
            newPosition = Vector3.MoveTowards(newPosition, end, webCastSpeed);
            _webRend.SetPosition(1, newPosition);
            sqrRemainingDistance = (newPosition - end).sqrMagnitude;


            if (WebIsTeared(newPosition, false))
            {
                _canMove = true;
                StopCoroutine(_coroutineMoving);
                break;
            }
            yield return null;
        }

        /*if (raycastHit2D.collider.isTrigger)
        {
            PlayTearWebAnimation(end);
            _webRend.enabled = false;
            _canMove = true;
            yield return null;
        }
        else
        {*/
            yield return SmoothPull(end);
        //}
    }

    //pull the player to target smoothly
    protected IEnumerator SmoothPull(Vector3 end)
    {
        float magnitude = (end - transform.position).magnitude;
        _dJoint2d.enabled = true;
        _dJoint2d.distance = magnitude;
        _dJoint2d.connectedAnchor = end;

        //play animation
        if (end.y > transform.position.y)
            _animator.SetBool("ClimbUp", true);
        else
            _animator.SetBool("ClimbDown", true);

        yield return null;

        while (_dJoint2d.distance > minDistance)
        {
            float speed = pullSpeed;

            //speed up when player stucks
            if (_rb2d.velocity.sqrMagnitude < float.Epsilon)
            {
                speed += .1f;
                _dJoint2d.connectedAnchor = _dJoint2d.connectedAnchor;
            }

            _dJoint2d.distance = Mathf.Clamp(_dJoint2d.distance - speed, minDistance, magnitude);

            if (WebIsTeared(_dJoint2d.connectedAnchor, true))
            {
                yield return WaitWhileFalling();
                break;
            }

            yield return null;
        }

        yield return AlignThePlayer();
    }

    // align the player along the earth
    protected IEnumerator AlignThePlayer()
    {
        StopClimbAnimation();

        Quaternion targetRotation = Quaternion.Euler(0, 0, 0);

        float lastAngle = Quaternion.Angle(targetRotation, transform.rotation);

        while (transform.rotation != targetRotation)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, alignSpeed);

            yield return null;

            float newAngle = Quaternion.Angle(targetRotation, transform.rotation);

            if (lastAngle < newAngle)
            {
                break;
            }

            lastAngle = newAngle;
        }

        _canMove = true;
    }

    //wait while player is falling
    protected IEnumerator WaitWhileFalling()
    {
        while (_rb2d.velocity != Vector2.zero && _rb2d.angularVelocity != 0)
        {
            yield return new WaitForSeconds(.2f);
        }
    }
    #endregion

    #region protected methods

    //update player positin
    protected void UpdatePosition()
    {
        bool isTop = Physics2D.Linecast(transform.position, checkTop.position, whatIsGround);
        bool current = _animator.GetBool("Grounded");

        if (isTop && current)
        {
            _animator.SetBool("Grounded", false);
            _animator.SetBool("OnTheTop", true);
        }
        if (!isTop && !current)
        {
            _animator.SetBool("Grounded", true);
            _animator.SetBool("OnTheTop", false);
        }
    }

    //freeze the player
    protected void Freeze()
    {
        if(_coroutineMoving != null)
            StopCoroutine(_coroutineMoving);
        _canMove = false;
        _dJoint2d.enabled = false;
        _rb2d.bodyType = RigidbodyType2D.Kinematic;
        _rb2d.velocity = Vector2.zero;
        _rb2d.freezeRotation = true;
    }
    //play wed is teared animation
    protected void PlayTearWebAnimation(Vector2 point)
    {
        if (webExplosion)
        {
            Instantiate(webExplosion, point, transform.rotation);
        }
    }

    //is valid mouse point of not (ignore ui)
    protected bool IsValidMousePoint(Vector2 touch)
    {
        foreach(GameObject uiElement in uiInteractives)
        {
            if (uiElement.activeInHierarchy)
            {
                RectTransform rect = uiElement.GetComponent<RectTransform>();
                if(RectTransformUtility.RectangleContainsScreenPoint(rect, touch))
                {
                    return false;
                }
            }
        }
        return true;
    }

    //move the player
    protected void DoMove()
    {
        if (!_touchStarted && Input.GetMouseButtonDown(0))
        {
            
            if (IsValidMousePoint(Input.mousePosition))
            {
                _touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _touchStarted = true;
                arrow.SetActive(true);
                LookToMoveDirection(_touchStart);

                //visual the touch
                if (onTouchVisual)
                {
                    _onTouchVisualExmp = Instantiate(onTouchVisual, _touchStart, Quaternion.Euler(0, 0, 0));
                    _onTouchLine = _onTouchVisualExmp.GetComponent<LineRenderer>();
                    _onTouchLine.SetPosition(0, _touchStart);
                    _onTouchLine.SetPosition(1, _touchStart);
                }
            }

        }
        else if (_touchStarted)
        {
            _touchEnd = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            LookToMoveDirection(_touchEnd - _touchStart);

            //visual the touch
            if (_onTouchLine)
            {
                _onTouchLine.SetPosition(1, _touchEnd);
            }

        }

        if (Input.GetMouseButtonUp(0) && _touchStarted)
        {
            _touchStarted = false;
            CastWeb();
            arrow.SetActive(false);

            //visual the touch
            if (_onTouchVisualExmp)
            {
                Destroy(_onTouchVisualExmp);
                _onTouchLine = null;
            }
        }
    }

    //update web each frame
    protected void UpdateWeb()
    {
        if (_webRend.enabled)
        {
            _webRend.SetPosition(0, transform.position);
        }
    }

    //cast web to direction
    protected void CastWeb()
    {
        RaycastHit2D[] results = new RaycastHit2D[1];
        Vector2 webDirection = new Vector2(arrowSprite.position.x - transform.position.x, arrowSprite.position.y - transform.position.y);

        if (_coll2d.Raycast(webDirection, results, Mathf.Infinity, whatIsGround) > 0)
        {
            PlaySfx(webSound);
            _coroutineMoving = StartCoroutine(SmoothWebDrawning(results[0]));
        }
    }

    //arrow points to direction
    protected void LookToMoveDirection(Vector2 direction)
    {
        float rot_z = Vector2.SignedAngle(Vector2.up, direction);

        float legal = getLegalAngle(rot_z);

        //Debug.Log ("rot_z: " + rot_z + " rot_z legal: " + legal);

        arrow.transform.rotation = Quaternion.Euler(0f, 0f, legal);
    }

    //get legal angle (the player can't cast web to ground)
    protected float getLegalAngle(float z)
    {
        bool isLeft = Physics2D.Linecast(transform.position, checkLeft.position, whatIsGround);

        bool isBottom = Physics2D.Linecast(transform.position, checkBottom.position, whatIsGround);

        bool isRight = Physics2D.Linecast(transform.position, checkRight.position, whatIsGround);

        bool isTop = Physics2D.Linecast(transform.position, checkTop.position, whatIsGround);

        if (isLeft)
        {
            if (z > 0)
                z = z >= 90 ? 180 : 0;
        }

        if (isRight)
        {
            if (z < 0)
                z = z >= -90 ? 0 : 180;
        }

        if (isBottom)
        {
            if (z < -90 || z > 90)
                z = z < -90 ? -90 : 90;
        }

        if (isTop)
        {
            if (z < 90 && z > -90)
                z = z > 0 ? 90 : -90;
        }

        if (isBottom && isLeft)
        {
            if (z > 0 || z < -90)
                z = z > 135 || z < -90 ? -90 : 0;
        }

        if (isBottom && isRight)
        {
            if (z < 0 || z > 90)
                z = z < -135 || z > 90 ? 90 : 0;
        }

        if (isTop && isRight)
        {
            if (z < 90)
                z = z < -45 ? 180 : 90;
        }

        if (isTop && isLeft)
        {
            if (z > -90)
                z = z < 45 ? -90 : 180;
        }

        return z;
    }

    //turn off climbing animation
    protected void StopClimbAnimation()
    {
        _animator.SetBool("ClimbUp", false);
        _animator.SetBool("ClimbDown", false);
    }

    //tear the web if there a barrier on the way
    protected bool WebIsTeared(Vector2 endOfWeb, bool needFall)
    {
        RaycastHit2D[] results = new RaycastHit2D[1];
        Vector2 direction = endOfWeb - (Vector2)transform.position;

        if (_coll2d.Raycast(direction, results, direction.magnitude - .1f, whatCanTearWeb) > 0)
        {
            if (needFall)
            {
                _webRend.enabled = false;
                _dJoint2d.enabled = false;
            }
            else
            {
                if (_dJoint2d.enabled)
                {
                    _webRend.SetPosition(1, _dJoint2d.connectedAnchor);
                }
                else
                {
                    _webRend.enabled = false;
                }                
            }

            PlayTearWebAnimation(results[0].point);
            PlaySfx(webRipping);
            StopClimbAnimation();

            return true;
        }

        return false;
    }
    #endregion
}
