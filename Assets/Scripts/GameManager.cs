﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour {

    public GameObject UIGameOver;
    public GameObject UIVictory;
    public GameObject UIPause;

    public GameObject UILastLevel;
    public bool isLastLevel;

    public TextMeshProUGUI scoreText;
    public string nextLevel;
    public string mainMenu;
    public AudioSource backgroundMusicSource;
    public SceneFader fader;
    public GameObject coins;
    public GameObject stars;

    protected int _score;
    protected int _maxScore;
    protected bool _paused;
    protected KongregateAPIBehaviour _kongregate;
    public Animator[] _stars;

    #region unity messages
    // Use this for initialization
    void Start () {
        UIGameOver.SetActive(false);
        UIGameOver.SetActive(false);
        _score = 0;
        _paused = false;
        _maxScore = CountMaxScore();
        _stars = stars.GetComponentsInChildren<Animator>();
        UpdateScoreText();
        _kongregate = GameObject.FindGameObjectWithTag("KongregateAPI")
                                .GetComponent<KongregateAPIBehaviour>();

        AudioListener.volume = PrefManager.IsSoundOn() ? 1 : 0;
    }
	
	// Update is called once per frame
	void Update () {

        //pause game by esc
		if (Input.GetKeyDown(KeyCode.Escape)){
            Pause();
        }
	}
    #endregion

    #region public methods
    //call if victory
    public void Victory()
    {
        if (!UIGameOver.activeInHierarchy && !UIVictory.activeInHierarchy)
        {
            _paused = true;
            UIVictory.SetActive(true);
            backgroundMusicSource.Stop();

            //counting stars
            int starsCount = _score/ (_maxScore / _stars.Length);
            //make stars look gained
            for(int i = 0; i < starsCount; i++)
            {
                _stars[i].SetTrigger("Gained");
            }

            //save progress
            string scene = SceneManager.GetActiveScene().name;
            if (PlayerPrefs.GetInt(scene) < starsCount)
            {
                PlayerPrefs.SetInt(scene, starsCount);
            }

            //unlock next level
            if (!PrefManager.IsUnlocked(nextLevel))
            {
                PrefManager.UnlockLevel(nextLevel);
            }

            if(_kongregate != null)
            {
                _kongregate.UpdateStarsCount(CountStars());
            }
        }
    }

    public void UpdateScore(int score)
    {
        _score += score;
        UpdateScoreText();
    }

    //call if game over
    public void GameOver()
    {
        if (!UIGameOver.activeInHierarchy && !UIVictory.activeInHierarchy)
        {
            _paused = true;
            UIGameOver.SetActive(true);
            backgroundMusicSource.Stop();
        }
    }

    //load next level
    public void LoadNextLevel()
    {
        if (isLastLevel)
        {
            UIVictory.SetActive(false);
            UILastLevel.SetActive(true);
        }else
            SceneManager.LoadScene(nextLevel);
    }

    //restart the level
    public void RestartLevel()
    {

        ResumeOnlyTime();
        fader.FadeTo(SceneManager.GetActiveScene().name);
    }

    //load main menu
    public void ToMainMenu()
    {
        ResumeOnlyTime();
        fader.FadeTo(mainMenu);
    }

    //is game paused?
    public bool IsGamePaused()
    {
        return _paused;
    }

    //pause game
    public void Pause()
    {
        if (!_paused)
        {
            _paused = true;
            Time.timeScale = 0f;
            backgroundMusicSource.Pause();
            UIPause.SetActive(true);
        }
    }

    //resume to game
    public void Resume()
    {
        if (_paused)
        {
            _paused = false;
            Time.timeScale = 1f;
            backgroundMusicSource.Play();
            UIPause.SetActive(false);
        }
    }

    public void ResumeOnlyTime()
    {
        if (_paused)
        {
            Time.timeScale = 1f;
        }
    }
    #endregion

    #region protected methods
    protected void UpdateScoreText()
    {
        scoreText.SetText(_score.ToString());
    }

    //count max score
    protected int CountMaxScore()
    {
        int ret = 0;
        Pickable[] pickables = coins.GetComponentsInChildren<Pickable>();
        for (int i = 0; i < pickables.Length; i++)
        {
            ret += pickables[i].score;
        }
        return ret;
    }

    protected int CountStars()
    {
        int count = SceneManager.sceneCountInBuildSettings;
        int ret = 0;
        for(int i = 0; i < count; i++)
        {
            string scene = SceneUtility.GetScenePathByBuildIndex(i);
            scene = GetSceneName(scene);
            ret += PrefManager.GetStars(scene);
        }
        Debug.Log("stars count: " + ret);
        return ret;
    }

    protected string GetSceneName(string path)
    {
        int start = path.LastIndexOf("/");
        start = start == -1 ? path.LastIndexOf("\\") : start;
        start++;
        int l = path.LastIndexOf(".unity") - start;
        return path.Substring(start, l);
    }

    #endregion
}
