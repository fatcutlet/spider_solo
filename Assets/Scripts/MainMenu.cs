﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public Animator soundButton;

    private void Start()
    {
        soundButton.SetBool("Sound", PrefManager.IsSoundOn());
        AudioListener.volume = PrefManager.IsSoundOn() ? 1 : 0;
		TestSaves();
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ChangeSoundState()
    {
        if (PrefManager.IsSoundOn())
        {
            soundButton.SetBool("Sound", false);
            PrefManager.SetSoundState(false);
            AudioListener.volume = 0;
        }
        else
        {
            soundButton.SetBool("Sound", true);
            PrefManager.SetSoundState(true);
            AudioListener.volume = 1;
        }
    }

	public void TestSaves()
	{
		string dataPath = Application.persistentDataPath + "\\test";
		Debug.Log("data path: " + dataPath);

		if (File.Exists(dataPath))
		{
			Debug.Log("Data file esists");
			try
			{
				using (StreamReader sr = new StreamReader(dataPath))
				{
					string data = sr.ReadToEnd();
					Debug.Log("Readed data: " + data);
				}
			}
			catch (Exception e)
			{
				Debug.Log(e.StackTrace);
			}
		}
		else
		{
			Debug.Log("Data file does not exist");

			File.WriteAllText(dataPath, "cyka blyat");
			Debug.Log("File was filled");
		}
	}
}
