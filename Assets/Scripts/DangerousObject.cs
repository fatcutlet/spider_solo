﻿using UnityEngine;
using System.Collections;

public class DangerousObject : MonoBehaviour
{
    public int damage = 1;

    #region unity messages
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            //damage the player
            Player player = collision.GetComponent<Player>();
            if(player.isAlive)
            player.ApplyDamage(damage);
        }
    }
    #endregion
}
