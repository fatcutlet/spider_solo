﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{

    public Transform objectToMove;
    public Transform[] waypoints;
    public int startWayPointIndex = 0;
    [Range(0f, 5f)]
    public float moveSpeed = 5f;
    public float waitAtWaypointTime = 1f;
    public bool loop = true;

    Transform _transform;
    int _myWaypointIndex = 0;
    float _moveTime;
    bool _moving = true;

    #region unity messages
    void Start()
    {
        _transform = objectToMove;
        _moveTime = Time.time + waitAtWaypointTime;
        _moving = true;
    }

    void Update()
    {
        if (Time.time >= _moveTime)
        {
            Movement();
        }
    }
    #endregion

    #region protectod methods
    protected void Movement()
    {
        if ((waypoints.Length != 0) && (_moving))
        {
            _transform.position = Vector3.MoveTowards(_transform.position, waypoints[_myWaypointIndex].transform.position, moveSpeed * Time.deltaTime);

            if (Vector3.Distance(_transform.position, waypoints[_myWaypointIndex].position) <= 0)
            {
                _myWaypointIndex++;
                _moveTime = Time.time + waitAtWaypointTime;
            }

            if (_myWaypointIndex >= waypoints.Length)
            {
                if (loop)
                {
                    _myWaypointIndex = 0;
                }
                else
                {
                    _moving = false;
                }
            }
        }
    }
    #endregion
}
