﻿using UnityEngine;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour {

    public SceneFader fader;

    public GameObject[] levelButtons;

    private void Start()
    {
        UpdateButtons();
    }


    public void Select(string levelname)
    {
        fader.FadeTo(levelname);
    }

    //update buttons according progress
    protected void UpdateButtons()
    {
        foreach(GameObject button in levelButtons)
        {
            LevelButton lb = button.GetComponent<LevelButton>();
            Button b = button.GetComponent<Button>();
            GameObject[] stars = lb.starts;
            string scene = lb.levelName;

            if (PrefManager.IsUnlocked(scene))
            {
                int starsCount = PrefManager.GetStars(scene);
                for (int i = 0; i < stars.Length; i++)
                {
                    stars[i].SetActive(i < starsCount);
                }
            }
            else
                b.interactable = false;
        }
    }
}
