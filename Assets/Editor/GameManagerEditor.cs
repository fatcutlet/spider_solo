﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameManager myGM = (GameManager)target;

        if (GUILayout.Button("Show progress"))
        {
            Show();
        }
        if (GUILayout.Button("Unlock next level"))
        {
            PrefManager.UnlockLevel(myGM.nextLevel);
        }
        if (GUILayout.Button("Unlock this level"))
        {
            PrefManager.UnlockLevel(SceneManager.GetActiveScene().name);
        }
        if (GUILayout.Button("Unlock all levels"))
        {
            UnlockAllLevels();
        }
        if (GUILayout.Button("Reset progress"))
        {
            PrefManager.Reset();
        }
    }

    //getting a name of a scene from path
    public static string GetSceneName(string path)
    {
        int start = path.LastIndexOf("/");
        start = start == -1 ? path.LastIndexOf("\\") : start;
        start++;
        int l = path.LastIndexOf(".unity") - start;
        return path.Substring(start, l);
    }

    public static void UnlockAllLevels()
    {
        EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
        foreach (EditorBuildSettingsScene scene in scenes)
        {

            string sceneName = GetSceneName(scene.path);

            //unlock the scene
            if (!PrefManager.IsUnlocked(sceneName))
            {
                PrefManager.UnlockLevel(sceneName);
            }
        }
    }

    public static void Show()
    {
        Debug.Log("Current progress layout");
        EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
        foreach (EditorBuildSettingsScene scene in scenes)
        {
            string sceneName = GetSceneName(scene.path);
            if (PrefManager.IsUnlocked(sceneName))
            {
                int stars = PrefManager.GetStars(sceneName);
                Debug.Log("Scene: " + sceneName + " Stars: " + stars);
            }
        }
    }
}
